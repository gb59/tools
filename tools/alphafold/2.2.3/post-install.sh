#!/bin/bash

version=2.2.3

set +e

eval "$(${CONDA_PREFIX}/bin/conda shell.bash hook)"

conda activate alphafold-${version}
if [ $? -ne 0 ]
then
    conda activate ${CONDA_PREFIX}/envs/alphafold-${version}
fi

if [ 'alphafold-'${version} != "${CONDA_DEFAULT_ENV}" ]
then
    echo "Unable to load alphafold env"
    exit 1;
fi

set -e

# for predictions on proteins that would typically be too long to fit into GPU memory
export TF_FORCE_UNIFIED_MEMORY="1"
export XLA_PYTHON_CLIENT_MEM_FRACTION="4.0"

# download & apply patch
cd ${CONDA_PREFIX}/lib/python3.8/site-packages/
wget -N https://raw.githubusercontent.com/deepmind/alphafold/v${version}/docker/openmm.patch
patch -p0 -N < openmm.patch || true

# recompile patched lib
cd ${CONDA_PREFIX}/lib/python3.8/site-packages/simtk/openmm/app/
python -m compileall .

# get chemical props
cd ${CONDA_PREFIX}/lib/python3.8/site-packages/alphafold/common/
wget -N https://git.scicore.unibas.ch/schwede/openstructure/-/raw/7102c63615b64735c4941278d92b554ec94415f8/modules/mol/alg/src/stereo_chemical_props.txt

# get launcher
cd ${CONDA_PREFIX}/bin/
wget -N https://raw.githubusercontent.com/deepmind/alphafold/v${version}/run_alphafold.py

#create run_alphafold.sh
echo '#!/bin/bash

python '${CONDA_PREFIX}'/bin/run_alphafold.py "$@"' > ${CONDA_PREFIX}/bin/run_alphafold.sh

chmod +x ${CONDA_PREFIX}/bin/run_alphafold.sh
